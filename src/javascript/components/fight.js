import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) {
    return damage;
  } else {
    return 0;
  }
}

export function getHitPower(fighter) {
  return fighter.attack * getCriticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * getDodgeChance();
}

function getCriticalHitChance() {
  const min = Math.ceil(1);
  const max = Math.floor(2);
  return (Math.random() * (max - min)) + min; 
}

function getDodgeChance() {
  return getCriticalHitChance();
}