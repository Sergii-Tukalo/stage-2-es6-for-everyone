import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter != null) {
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(createFighterInfo(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const attributes = {};

  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
    attributes,
  });

  const nameElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___name',
    attributes,
  });
  nameElement.append('Name: ' + name);

  const healthElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___health',
    attributes,
  });
  healthElement.append('Health: ' + health);

  const attackElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___attack',
    attributes,
  });
  attackElement.append('Attack: ' + attack);

  const defenseElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___defense',
    attributes,
  });
  defenseElement.append('Defense: ' + defense);

  infoElement.append(nameElement);
  infoElement.append(healthElement);
  infoElement.append(attackElement);
  infoElement.append(defenseElement);

  return infoElement;
}
