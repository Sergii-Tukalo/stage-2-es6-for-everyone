import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({title: 'Fight finished', bodyElement: `${fighter.name} Wins`});
}
