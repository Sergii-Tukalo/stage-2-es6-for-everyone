import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';
import { fight, getDamage, getHitPower, getBlockPower } from './fight';

export function renderArena(selectedFighters) {
  const fighterOne = selectedFighters[0];
  fighterOne.remainingHealth = fighterOne.health;
  const fighterSecond = selectedFighters[1];
  fighterSecond.remainingHealth = fighterSecond.health;

  const root = document.getElementById('root');

  reloadArena(root, selectedFighters);

  document.addEventListener('keypress', function (e) {
    e = e || window.event;

    turn(e.code, fighterOne, fighterSecond);

    const arena = reloadArena(root, selectedFighters);

    if (fighterOne.remainingHealth <= 0) {
      showWinnerModal(fighterSecond);
    } else if (fighterSecond.remainingHealth <= 0) {
      showWinnerModal(fighterOne);
    }
  }, false);
}

function reloadArena(root, selectedFighters) {
  const arena = createArena(selectedFighters);
  root.innerHTML = '';
  root.append(arena);
  return arena;
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name, health } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const width = 100 / fighter.health * fighter.remainingHealth;
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `$   {position}-fighter-indicator`, style: `width: ${width}%` }});
  
  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function turn(keyCode, fighterOne, fighterSecond) {
  if (keyCode == controls.PlayerOneAttack) {
    const damage = getDamage(fighterOne, fighterSecond);
    fighterSecond.remainingHealth -= damage;
  } else if (keyCode == controls.PlayerTwoAttack) {
    const damage = getDamage(fighterSecond, fighterOne);
    fighterOne.remainingHealth -= damage;
  }
}
